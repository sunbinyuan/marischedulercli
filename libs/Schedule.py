class Schedule:
    """Schedule class
    """

    DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    DAYS_PREF = ["M", "T", "W", "H", "F", "S", "D"]

    def __init__(self, days=7, starttime=0, endtime=1440, div=30):
        self.days = days
        self.starttime = starttime
        self.endtime = endtime
        self.div = div
        self.grid = [0 for i in range(self.days * (self.endtime - self.starttime) // self.div)]
        self.entries = []

    def copy(self):
        """ Make a copy of the object """
        copy = Schedule(self.days, self.starttime, self.endtime, self.div)
        copy.grid = self.grid[::]
        copy.entries = self.entries[::]

        return copy

    def __add__(self, other):
        """ Add two Schedule objects by merging the entries list 
        and populate the grid schedule with the most entries with the 
        one with the least entries """
        lenSelf = len(self.entries)
        lenOther = len(other.entries)
        if lenSelf > lenOther:
            copy = self.copy()
            for entry in other.entries:
                copy.addEntry(entry)
        else:
            copy = other.copy()
            for entry in self.entries:
                copy.addEntry(entry)
        return copy

    def addEntry(self, entry):
        """ Add an entry in the schedule and populate it from the schedule grid """
        self.entries.append(entry)
        for time in entry.times:
            day = time['day']
            hour = time['time']
            self[self.DAYS_PREF.index(day), hour[0], hour[1]] = 1

    def removeEntry(self, entry):
        """ Remove an entry in the schedule and unpopulate it from the schedule grid """
        for el in self.entries:
            if el == entry:
                self.entries.remove(el)
                for time in entry.times:
                    day = time['day']
                    hour = time['time']
                    self[self.DAYS_PREF.index(day), hour[0], hour[1]] = 0
                break

    def __getitem__(self, *key):
        """ Get the value of an item in the grid
        Format: schedule[day, time] """
        if len(key) != 2:
            return None

        else:
            day = key[0]
            time = key[1]

            if day > self.days:
                return None

            if time < self.starttime or time >= self.endtime:
                return None

            return self.grid[day * (self.endtime- self.starttime) // self.div \
            + (time-self.starttime) // self.div]

    def __setitem__(self, key, value):
        """ Change the value of a item in the grid
        Format: schedule[day, time] = value """
        if len(key) != 2 and len(key) != 3:
            return None
        else:
            day = key[0]
            time = key[1]
            endtime = key[2] if len(key) == 3 else time + self.div

            if day > self.days:
                return None

            if time < self.starttime or time >= self.endtime:
                return None

            if endtime < time or endtime >= self.endtime:
                return None

            while time < endtime:
                index = day * (self.endtime- self.starttime) // self.div \
                + (time-self.starttime) // self.div
                assert(not self.grid[index] or value == 0)
                self.grid[index] = value
                time += self.div
    
    def __repr__(self):
        """ Representation of the Schedule in a list form """
        return str(self.grid)

    # def __str__(self):
    #     """ Representation of the Schedule in a table form """
    #     text = str(("{:<10s}"+"{:<10s}"*self.days).format("", *self.DAYS))
    #     text += "\n"
    #     for i in range((self.endtime - self.starttime) // self.div):
    #         text += str(("{:<10s}"+"{:<10s}"*self.days).format(Schedule.timeToStr(i*self.div + self.starttime), \
    #             *[str(self.grid[i + j*(self.endtime - self.starttime) // self.div]) for j in range(self.days)]))
    #         text += "\n"
    #     return text

    def __str__(self):
        """ Representation of the Schedule in a table form """
        text = "|{:^11s}|".format(" /// ") + str(("|{:^10s}|"*self.days).format(*self.DAYS))
        text += "\n"
        for i in range((self.endtime - self.starttime) // self.div):
            text += "|{:^10s}|".format("-".join([Schedule.timeToStr(i*self.div + self.starttime),Schedule.timeToStr((i+1)*self.div + self.starttime)]))
            text += str("|{:^10s}|"*self.days).format( *[str(self.grid[i + j*(self.endtime - self.starttime) // self.div]) for j in range(self.days)])
            text += "\n"
        return text

    @staticmethod
    def timeToStr(time):
        """ Convert time (in min) into a string """
        return "{:02d}:{:02d}".format(time//60, time%60)

    @staticmethod
    def strToTime(st):
        """ Convert string into time (in min) """
        times = [int(el) for el in st.split(':')]
        return times[0]*60 + times[1]

if __name__ == "__main__":
    a = Schedule(starttime=495, endtime=1095, days=5)
    print(a)
    print(a.__repr__())
    a[1, 500] = 1
    b = a.copy()
    b[3, 500] = 1
    print(a)
    print(b)
    print(Schedule.timeToStr(495))
    print(Schedule.strToTime('12:45'))
    
