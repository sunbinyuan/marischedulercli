import json
from .Schedule import Schedule
class Entry:
    """Entry Object that store info of a course into an container """

    def __init__(self, name, teacher, code, section, times):
        self.name = name
        self.teacher = teacher
        self.code = code
        self.section = section
        self.times = times
    
    def __str__(self):
        st = "Name: {} \n".format(self.name)
        st += "Teacher: {} \n".format(self.teacher)
        st += "Code: {} \n".format(self.code)
        st += "Section: {} \n".format(self.section)
        st += "Times: {}".format(self.prettifyTimes())
        return st

    def prettifyTimes(self):
        """Used to pretty self.times
        st += "Times: {}".format(self.prettyTimes())
           ex: Turns {'F':[495,525]} to F: 8:15-8:45"""
        st = ""
        for timedayDict in self.times:
            st += timedayDict["day"] + ": "
            st += "-".join([Schedule.timeToStr(time) for time in timedayDict["time"]]) + " "
        return st

    def __cmp__(self, other):
        """ Compare if two entry are the same
            Only section and code ID are checked since a schedule cannot have
            the same class twice even with a different teacher/times
        """
        return self.section == other.section and self.code == other.code

    @staticmethod
    def jsonParse(file):
        """ Parse the json file into a dictionary of courses stored by the course id """
        parsed = None
        with open(file, "r") as f:
            parsed = json.loads(f.read())
        courses = {}
        for entry in parsed:
            teacher = entry["teacher"]
            course = entry["code"]
            # Add course entry if not exist
            if not course in courses.keys():
                courses[course] = []

            section = entry["section"]
            description = entry["name"]        
            arr_meetings = entry["meeting"]

            arr_time = []
            for meeting in arr_meetings:
                hours = [Schedule.strToTime(hour) for hour in meeting["time"]]
                    
                for c in meeting["day"]:
                    arr_time.append({"day": c, "time": hours})

            obj = Entry(description, teacher, course, section, arr_time)
            courses[course].append(obj)
        return courses
