from .Schedule import Schedule

class ScheduleViewer(Schedule):
    """Inherits from the Schedule Class but has different grid and entries attributes, a different addEntry method and a different __str__ method
    """
    
    def __init__(self,aSched,width = 11):
        """Input: An object of the Schedule class and a width of minimum 11 for printing purposes
        """    
        self.width = width
        self.days = aSched.days
        self.starttime = aSched.starttime
        self.endtime = aSched.endtime
        self.div = aSched.div
        self.grid = ["" for i in range(self.days * (self.endtime - self.starttime) // self.div)]
        self.entries = []
        
        #Adding all the entries of aSched to the Schedule Viewer using the addEntry method:
        for entry in aSched.entries:
            self.addEntry(entry) 
    
    def addEntry(self, entry):
        """ Add an entry in the schedule and populate the schedule grid
            NOTE: This version of addEntry populates the grid with the name of the entry, instead of with 1
            REMINDER: 1 only meant that the grid was occupied, but did not provide other information
        """
        self.entries.append(entry)

        for time in entry.times:
            day = time['day']
            hour = time['time']
            space = (hour[1] - hour[0]) // self.div 
            name = []
            for i in range(0, len(entry.name), self.width):
                name.append(entry.name[i:i+self.width])
            while len(name) != space:
                if len(name) > space:
                    name.pop()
                elif len(name) < space:
                    name.append("(cont.)")
            for i in range(space):
                self[self.DAYS_PREF.index(day), hour[0] +  i*self.div] = name[i] #Changed to entry.name instead of 1

    def __str__(self):
        """ Representation of the Schedule in a table form
        """
        WIDTH = self.width
        text = ""
        
        #Concatenating the row of days to text:
        daysRow = "|{:^{}s}|".format("///", WIDTH) + str(("|{:^" + str(WIDTH) +"s}|")*self.days).format(*self.DAYS) + "\n"
        text += daysRow
        
        for i in range((self.endtime - self.starttime) // self.div):
            #Concatenating the timerange grid to text:
            timerangeGrid = "|{:^{}s}|".format("-".join([Schedule.timeToStr(i*self.div + self.starttime),Schedule.timeToStr((i+1)*self.div + self.starttime)]), WIDTH)
            text += timerangeGrid

            #Concatenating the row of classes to text:
            classRow = ""
            for j in range(self.days):
                className = str(self.grid[i + j*(self.endtime - self.starttime) // self.div])
                if len(className) > WIDTH:
                    className = className[:WIDTH - 3] + "..."        #WIDTH-3 since we want space for the "..."
                classGrid = "|{:^{}s}|".format(className, WIDTH)
                classRow += classGrid
            text += classRow + "\n"

        return text

    def getDetails(self):
        st = "---------------YOUR CLASSES---------------\n"
        for entry in self.entries:
            st += str(entry)
            st += "\n------------------------------------------\n"
        return st
