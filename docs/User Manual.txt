How to use:
1. Run Main.py. Opening Courses Reference.pdf is also recommended.
2. Prompts will ask for commands
List of Commands:

	addCourse <CourseID> : Adds the course corresponding to this course ID to your schedule.
	
	delCourse <CourseID> : Deletes the course corresponding to this course ID from your schedule and the class if you picked a class from this course.

	addClass <CourseID> <SectionID> : Adds the course and class corresponding to this course and section ID to your schedule.

	delClass <CourseID> <SectionID> : Deletes the class corresponding to this course and section ID from your schedule, but keeps the course.

	automate <amount> : Prints an amount of possible schedule configurations.
			    A prompt will ask if you wish to choose a schedule amongst those shown,
			    Input the ID (Shown with the schedules) of the desired schedule to choose it.
			    Input 0 for none.

	mySched : Prints your personal schedule.

	list (courses|classes) : Prints out either the courses or the classes added to your schedule.

        save [path] : Save the schedule at path. If no path is entered, the default path "./schedule.txt" is choosen.

        help : Prints the list of commands.

	credits : Prints out the credits.

NOTE: All CourseID's and SectionID's can be found inside Courses Reference.pdf along with extra information on all the classes.
NOTE: If the amount inputed for automate is higher than the number of possible configurations, automate will only print that number.

ex:

Welcome to the console based schedule maker for Marianopolis
Enter 'help' to get started
Input a command: addCourse 603-ACB-MS
603-ACB-MS is added
Input a command: delCourse 603-ACB-MS
603-ACB-MS is removed from the course list
Input a command: addClass 603-ACB-MS 1
603-ACB-MS is added
Class successfully added
Input a command: delClass 603-ACB-MS 1
Class successfully removed
Input a command: addCourse 101-LCU-05
101-LCU-05 is added
Input a command: automate 2
ID #1
|    ///    ||  Monday   ||  Tuesday  || Wednesday || Thursday  ||  Friday   || Saturday  ||  Sunday   |
|08:15-08:45||           ||           ||           ||           ||           ||           ||           |
|08:45-09:15||           ||           ||           ||           ||           ||           ||           |
|09:15-09:45||           ||           ||           ||           ||           ||           ||           |
|09:45-10:15||           ||           ||           ||           ||           ||           ||           |
|10:15-10:45||           ||CREATIVE NO||           ||           ||CREATIVE NO||           ||           |
|10:45-11:15||           ||NFICTION: W||           ||           ||NFICTION: W||           ||           |
|11:15-11:45||           ||RITING (TRU||           ||           ||RITING (TRU||           ||           |
|11:45-12:15||           ||E) STORIES ||           ||           ||E) STORIES ||           ||           |
|12:15-12:45||GENERAL BIO||           ||           ||           ||           ||           ||           |
|12:45-13:15||  LOGY II  ||           ||GENERAL BIO||           ||GENERAL BIO||           ||           |
|13:15-13:45||  (cont.)  ||           ||  LOGY II  ||           ||  LOGY II  ||           ||           |
|13:45-14:15||  (cont.)  ||           ||  (cont.)  ||           ||  (cont.)  ||           ||           |
|14:15-14:45||           ||           ||           ||           ||           ||           ||           |
|14:45-15:15||           ||           ||           ||           ||           ||           ||           |
|15:15-15:45||           ||           ||           ||           ||           ||           ||           |
|15:45-16:15||           ||           ||           ||           ||           ||           ||           |
|16:15-16:45||           ||           ||           ||           ||           ||           ||           |
|16:45-17:15||           ||           ||           ||           ||           ||           ||           |
|17:15-17:45||           ||           ||           ||           ||           ||           ||           |
|17:45-18:15||           ||           ||           ||           ||           ||           ||           |

ID #2
|    ///    ||  Monday   ||  Tuesday  || Wednesday || Thursday  ||  Friday   || Saturday  ||  Sunday   |
|08:15-08:45||           ||           ||           ||           ||           ||           ||           |
|08:45-09:15||           ||           ||           ||           ||           ||           ||           |
|09:15-09:45||           ||           ||           ||           ||           ||           ||           |
|09:45-10:15||           ||           ||           ||           ||           ||           ||           |
|10:15-10:45||           ||CREATIVE NO||           ||           ||CREATIVE NO||           ||           |
|10:45-11:15||           ||NFICTION: W||           ||           ||NFICTION: W||           ||           |
|11:15-11:45||           ||RITING (TRU||           ||GENERAL BIO||RITING (TRU||           ||           |
|11:45-12:15||           ||E) STORIES ||           ||  LOGY II  ||E) STORIES ||           ||           |
|12:15-12:45||           ||           ||           ||  (cont.)  ||           ||           ||           |
|12:45-13:15||           ||           ||           ||           ||           ||           ||           |
|13:15-13:45||           ||           ||           ||           ||           ||           ||           |
|13:45-14:15||           ||           ||           ||           ||           ||           ||           |
|14:15-14:45||           ||           ||           ||           ||           ||           ||           |
|14:45-15:15||           ||           ||           ||           ||           ||           ||           |
|15:15-15:45||           ||           ||           ||           ||           ||           ||           |
|15:45-16:15||           ||           ||           ||           ||           ||           ||           |
|16:15-16:45||GENERAL BIO||           ||GENERAL BIO||           ||           ||           ||           |
|16:45-17:15||  LOGY II  ||           ||  LOGY II  ||           ||           ||           ||           |
|17:15-17:45||  (cont.)  ||           ||  (cont.)  ||           ||           ||           ||           |
|17:45-18:15||           ||           ||           ||           ||           ||           ||           |

Choose a schedule by its ID? 1 for 1st, 2 for 2nd, etc 0 for neither: 1
Class successfully added
Class successfully added
Input a command: mySched
|    ///    ||  Monday   ||  Tuesday  || Wednesday || Thursday  ||  Friday   || Saturday  ||  Sunday   |
|08:15-08:45||           ||           ||           ||           ||           ||           ||           |
|08:45-09:15||           ||           ||           ||           ||           ||           ||           |
|09:15-09:45||           ||           ||           ||           ||           ||           ||           |
|09:45-10:15||           ||           ||           ||           ||           ||           ||           |
|10:15-10:45||           ||CREATIVE NO||           ||           ||CREATIVE NO||           ||           |
|10:45-11:15||           ||NFICTION: W||           ||           ||NFICTION: W||           ||           |
|11:15-11:45||           ||RITING (TRU||           ||           ||RITING (TRU||           ||           |
|11:45-12:15||           ||E) STORIES ||           ||           ||E) STORIES ||           ||           |
|12:15-12:45||GENERAL BIO||           ||           ||           ||           ||           ||           |
|12:45-13:15||  LOGY II  ||           ||GENERAL BIO||           ||GENERAL BIO||           ||           |
|13:15-13:45||  (cont.)  ||           ||  LOGY II  ||           ||  LOGY II  ||           ||           |
|13:45-14:15||  (cont.)  ||           ||  (cont.)  ||           ||  (cont.)  ||           ||           |
|14:15-14:45||           ||           ||           ||           ||           ||           ||           |
|14:45-15:15||           ||           ||           ||           ||           ||           ||           |
|15:15-15:45||           ||           ||           ||           ||           ||           ||           |
|15:45-16:15||           ||           ||           ||           ||           ||           ||           |
|16:15-16:45||           ||           ||           ||           ||           ||           ||           |
|16:45-17:15||           ||           ||           ||           ||           ||           ||           |
|17:15-17:45||           ||           ||           ||           ||           ||           ||           |
|17:45-18:15||           ||           ||           ||           ||           ||           ||           |

---------------YOUR CLASSES---------------
Name: CREATIVE NONFICTION: WRITING (TRUE) STORIES 
Teacher: Walser, Sabine 
Code: 603-ACB-MS 
Section: 00001 
Times: T: 10:15-12:15 F: 10:15-12:15 
------------------------------------------
Name: GENERAL BIOLOGY II 
Teacher: Oberholzer, Ursula 
Code: 101-LCU-05 
Section: 00002 
Times: M: 12:15-14:15 W: 12:45-14:15 F: 12:45-14:15 
------------------------------------------

Input a command: list classes
---------------YOUR CLASSES---------------
Name: CREATIVE NONFICTION: WRITING (TRUE) STORIES 
Teacher: Walser, Sabine 
Code: 603-ACB-MS 
Section: 00001 
Times: T: 10:15-12:15 F: 10:15-12:15 
------------------------------------------
Name: GENERAL BIOLOGY II 
Teacher: Oberholzer, Ursula 
Code: 101-LCU-05 
Section: 00002 
Times: M: 12:15-14:15 W: 12:45-14:15 F: 12:45-14:15 
------------------------------------------

Input a command: list courses
603-ACB-MS, 101-LCU-05
