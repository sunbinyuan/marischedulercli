from libs.Entry import Entry
from libs.Schedule import Schedule
from libs.ScheduleViewer import ScheduleViewer
from libs.Algorithm import Algorithm

import json

def writeCoursesFile():
    """ OPTIONAL: Use this to obtain a file with the info of all courses as an alternative to Courses Reference.pdf"""
    parsed = Entry.jsonParse('./libs/data.txt')
    with open("All courses reference.txt",'w') as f:
        for entriesList in parsed.values():
            for entry in entriesList:
                rep = "{} // {} // {} // {} // {}".format(str(entry.code),str(entry.section),str(entry.name),str(entry.teacher),str(entry.prettifyTimes()))
                f.write(rep +"\n")


def Main():

    parsed = Entry.jsonParse('./libs/data.txt')
    coursesDict = {}     #{ "courseID" : [<Entry Objects>,...] }
    schedule = Schedule(starttime=495, endtime=1095, days=7)

    def addCourse(courseID):
        """Adds a course to the user's personal schedule
           Input: The course's ID number
        """
        if courseID in coursesDict:
            print("Course already added")
        elif courseID not in parsed.keys():
            print("Course does not exist")
        else:
            try:
                coursesDict[courseID] = []
                print(courseID, "is added")
            except:
                print("Invalid Course")
            
    def delCourse(courseID):
        """Deletes a course from the user's personal schedule
           Input: The course's ID number
        """
        if courseID in coursesDict:
            classes = coursesDict.pop(courseID)
            for entry in classes:
                schedule.removeEntry(entry) #should only remove one entry
            print(courseID, "is removed from the course list")
        else:
            print("Not a picked course")
            
    def addClass(courseID,sectID):
        '''Adds a class to the user's personal schedule
           Input: The class's course ID number and section number
        '''
        if len(coursesDict.get(courseID,[])) >= 1:
            print("Class already exists for this course")
        else:
            try:
                if courseID not in coursesDict.keys(): #in case the course wasn't added yet
                    addCourse(courseID)
                zSectID = str(int(sectID)).zfill(5) #So that we can access the section attribute of Entry
                for entry in parsed[courseID]:
                    if entry.section == zSectID:
                        schedule.addEntry(entry) #populate it in the schedule
                        coursesDict[courseID].append(entry) #append Entry object to the course's list
                        print("Class successfully added")
                        break
            except AssertionError:
                print("This class overlap with some of your other classes")
            except:
                print("Invalid class")
            
    def delClass(courseID,sectID):
        '''Deletes a class to the user's personal schedule
           Input: The class's course ID number and section number
        '''
        zSectID = str(int(sectID)).zfill(5) #So that we can access the section attribute of Entry
        if courseID in coursesDict:
            for entry in coursesDict[courseID]:
                if entry.section == zSectID:
                    schedule.removeEntry(entry) #remove it from the schedule
                    coursesDict[courseID] = [] #resetting it to empty list
                    print("Class successfully removed")
        else:
            print("Not a picked course")

    def automate(amount):
        '''Prints an amount of possible schedules'''
        emptyKeys = [key for key in coursesDict.keys() if not coursesDict[key]] #make a list of all empty courses
        newCoursesDict = {}
        for course in emptyKeys:
            newCoursesDict[course] = parsed[course] #populate the new course dict with all the classes for the algorithm
        schedules = Algorithm.solveLimit(schedule.copy(), newCoursesDict, 0, 0, amount)
        for i in range(len(schedules)):
            print("ID #",i+1,sep="")
            print(ScheduleViewer(schedules[i]))

        def choose(schedules):
            '''Make user's schedule one of a given listen of schedules'''
            choice = input('Choose a schedule by its ID? 1 for 1st, 2 for 2nd, etc 0 for neither: ', )

            if choice == "0":
                print("You did not pick a schedule")
            elif int(choice) < 0:
                raise IndexError()
            else:
                for entry in schedules[int(choice)-1].entries:
                    if entry.code in emptyKeys:
                        addClass(entry.code,entry.section)
        for i in range(3): #attempt to retry 3 time in case of a bad input
            try:
                choose(schedules)
                break #break the loop if called successfully
            except IndexError:
                print("Invalid index") 
            except:
                print("Invalid input") #if the input isn't an integer
        return schedules

    def mySched():
        '''Prints out the user's schedule'''
        mySched = ScheduleViewer(schedule)
        print(mySched)
        print(mySched.getDetails())

    def processCommand(line):
        splitted = line.split(' ')
        command = splitted[0].lower()
        if command == "addcourse":
            if len(splitted) < 2:
                print("Syntax: addcourse <CourseID>")
            else:
                courseID = splitted[1]
                addCourse(courseID)
        elif command == "delcourse":
            if len(splitted) < 2:
                print("Syntax: delcourse <CourseID>")
            else:
                courseID = splitted[1]
                delCourse(courseID)
        elif command == "automate":
            if len(splitted) < 2:
                print("Syntax: automate <amount>")
            else:
                try:
                    amount = int(splitted[1])
                    automate(amount)
                except:
                    print("Please enter an integer")
        elif command == "addclass":
            if len(splitted) < 3:
                print("Syntax: addclass <CourseID> <section>")
            else:
                courseID = splitted[1]
                section = splitted[2]
                addClass(courseID, section)
        elif command == "delclass":
            if len(splitted) < 3:
                print("Syntax: delclass <CourseID> <section>")
            else:
                courseID = splitted[1]
                section = splitted[2]
                delClass(courseID, section)
        elif command == "schedule" or command == "mysched":
            mySched()
        elif command == "list":
            if len(splitted) < 2:
                print("Syntax: list (courses|classes)")
            else:
                subcommand = splitted[1].lower()
                if subcommand == "courses":
                    print(", ".join(coursesDict.keys()))
                elif subcommand == "classes":
                    print(ScheduleViewer(schedule).getDetails())
        elif command == "save":
            path = "./schedule.txt"
            if len(splitted) == 2:
                path = splitted[1]
            try:
                with open(path, "w") as f:
                    sw = ScheduleViewer(schedule)
                    f.write(str(sw))
                    f.write(sw.getDetails())
            except Exception:
                print("An error occurred when writting the file. Please make sure the path exists.")
            else:
                print("Schedule saved at", path)
            finally:
                f.close()
        elif command == "credits":
            print("Application made by Yuxuan Ji and Binyuan Sun")
        elif command == "exit":
            exit()
        elif command == "help" or command == "h":
            st = """
    List of Commands:

	addCourse <CourseID> : Adds the course corresponding to this course ID to your schedule.
	
	delCourse <CourseID> : Deletes the course corresponding to this course ID from your schedule and the class if you picked a class from this course.

	addClass <CourseID> <SectionID> : Adds the course and class corresponding to this course and section ID to your schedule.

	delClass <CourseID> <SectionID> : Deletes the class corresponding to this course and section ID from your schedule, but keeps the course.

	automate <amount> : Prints an amount of possible schedule configurations.
			    A prompt will ask if you wish to choose a schedule amongst those shown,
			    Input the ID (Shown with the schedules) of the desired schedule to choose it.
			    Input 0 for none.

	mySched : Prints your personal schedule.

	list (courses|classes) : Prints out either the courses or the classes added to your schedule.

        save [path] : Save the schedule at path. If no path is entered, the default path "./schedule.txt" is choosen.

        help : Prints the list of commands.

	credits : Prints out the credits.
            """
            print(st)
        else:
            print("Invalid command. Type help or h for help")


    while True:
        
        processCommand(input("Input a command: ", ))

        #Old method:
        # command = input("Input a command: ", )

        # if command == 'addCourse':
        #     courseID = input("Input the course ID: ",)
        #     addCourse(courseID)

        # elif command == 'automate':
        #     amount = int(input("Input the amount of schedules you wish to see: ",))
        #     schedules = automate(amount)

        # elif command == 'addClass':
        #     courseID = input("Input the course ID: ",)
        #     sectID = input("Input the section ID: ",)
        #     addClass(courseID,sectID)

        # elif command == 'delClass':
        #     courseID = input("Input the course ID: ",)
        #     sectID = input("Input the section ID: ",)
        #     delClass(courseID,sectID)

        # elif command == 'mySched':
        #     mySched()
            
        # else:
        #     print('Invalid Command')

if __name__ == '__main__':
    print("Welcome to the console based schedule maker for Marianopolis")                
    print("Type 'help' to get started")                
    Main()
