#How to use Schedule class example:
# sched = Schedule(15) where 15 is the size of the grids
# sched.add_Class("08:15-08:45","M","Linear Algebra 1")
# sched.del_Class("08:15-08:45","M")
from Entry import Entry
class Schedule():
    
    def __init__(self,width=15):
        self.times = ["08:15-08:45","08:45-09:15","09:15-09:45","09:45-10:15","10:15-10:45",
                     "10:45-11:15","11:15-11:45","11:45-12:15","12:15-12:45","12:45-13:15",
                     "13:15-13:45","13:45-14:15","14:15-14:45","14:45-15:15","15:15-15:45",
                     "15:45-16:15","16:15-16:45","16:45-17:15","17:15-17:45","17:45-18:15",
                     "18:15-18:45","18:45-19:15","19:15-19:45","19:45-20:15"]
        self.width = width #How wide you want each grid to be
        
        #The whole schedule is basically this Dict:
        #Keys: all timeranges from self.times
        #Key values: each slot correspond to a day and each slot can contain an Entry or "" -> no Entry
        self.sched = {time:["","","","","","",""] for time in self.times} 

    def getGrid(self,name,width = 15):
        """Returns a grid in this format:
           |name|
           Note: width is minimum 15 for visual purposes
                 name is a string, width is an int
        """
        if width < 6:
                print("Width must be longer")
                return 
        if len(name) >width - 2:                #Adds ... if the name is too long
                name = name [:width-5]          #-2 for the "|" and -3 for "..."
                name += "..."
        outp = "|{:^{}}|".format(name,width-2)  #"^" is justified alignment
        return outp

    def prtSched(self):
        """Prints out the schedule
        """
        ###### First we print a row of the days in a week ######
        days = "/// Monday Tuesday Wednesday Thursday Friday Saturday Sunday".split()
        rowDays = ""
        for day in days:
            rowDays += self.getGrid(day,self.width)
        print(rowDays)
        
        ###### Then we print the other rows ######
        for timerange in self.times:
            gridTimerange = self.getGrid(timerange,self.width) 
            outp = gridTimerange   #Right now, outp only has the timerange grid ex: |08:15-08:45|

            #row_classes will become the row of classes for that timerange
            rowClasses = ""
            for el in self.sched[timerange]:
                if isinstance(el,Entry):
                    rowClasses += self.getGrid(el.name,self.width)   #If the element is an Entry class, then we want to use it's name attribute
                else:
                    rowClasses += self.getGrid(el,self.width)        
           
            outp += rowClasses   #outp gets a class grid row on each iteration ex: |08:15-08:45| += |Linear...||         ||        ||...          
            print(outp)

    def addClass(self,day,timerange,classname):
        """Adds a class
           Input: timerange ex: "08:15-08:45"
                  day ex: "M"
                  classname ex: Linear Algebra 1
        """
        indexes = {"M": 0, "T":1, "W": 2,"H": 3, "F":4, "S":5, "D":6}
        '''times = entryClass.times                                                #ex: [{'day': 'W', 'time': ['14:15', '16:15']}, {'day': 'F', 'time': ['14:15', '16:15']}]
        days = [el["day"] for el in times]
        timeranges = ["-".join(listEl["time"]) for listEl in times]
        times = list(zip(days,timeranges))                                      #ex: [('W', '14:15-16:15'), ('F', '14:15-16:15')]
        classname = entryClass.name
        for day,timerange in times:'''
        try:
            #Check if day, timerange and classname are correct, and if the grid is not occupied
            assert day in indexes.keys()
            assert timerange in self.times
            assert isinstance(classname,str)
            index = indexes[day]
            assert not self.sched[timerange][index]

            #Changes self.sched's corresponding keyvalue slot to the classname
            self.sched[timerange][index] = classname
            
        except Exception as err:
            print("Invalid Input or Occupied Grid",err)
    
    def delClass(self,classname):
        """Deletes a class by looping over the keyvalues
        """
        for timerange,classes in self.sched.items():
            for i in range(len(classes)):
               if self.sched[timerange][i] == classname:
                   self.sched[timerange][i] = ""
                   
    def copy(self):
        """Creates a copy of the Schedule
        """
        outp = Schedule(self.width)
        outp.sched = self.sched
        return outp

    def getDetails(self, classname):
        """Prints out the details of a class
           If is of Entry class, will use it's __str__() method
           Else will just print the name
        """
        for timerange,classes in self.sched.items():
            for i in range(len(classes)):
               if self.sched[timerange][i] == classname:
                   print(self.sched[timerange][i])
                   return # Since printing it once is all we need
            
if __name__ == "__main__":
    test = Schedule(15)
    print("Empty Schedule with width 15:")
    test.prtSched()
    print("Adding Linear Algebra 1 at 08:15-08:45 on Monday:")
    test.addClass("08:15-08:45","M","Linear Algebra 1")
    test.prtSched()
    print("Deleting the class at 08:15-08:45 on Monday:")
    test.delClass("Linear Algebra 1")
    test.prtSched()
